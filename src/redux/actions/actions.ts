import { GET_MOST_POPULAR_MOVIES, GET_SEARCH_MOVIES, GET_MOVIE_DETAILS } from "./type"
import axios from 'axios'
export const mostPopular = (popularMovieList: any) => ({ type: GET_MOST_POPULAR_MOVIES, popularMovieList: popularMovieList })

export const getMostPopularMovies = () => (dispatch: any) => {
    axios(`https://api.themoviedb.org/3/movie/popular?api_key=138a341d99d20356c33e09261658afb3&language=pt-BR&page=1&region=us`)
        .then(result => dispatch(mostPopular(result.data.results)))
}


export const searchMovies = (searchMovieList: any) => ({ type: GET_SEARCH_MOVIES, searchMovieList: searchMovieList })


export const getSearchMovies = (search: string) => (dispatch: any) => {
    axios(`https://api.themoviedb.org/3/search/movie?api_key=138a341d99d20356c33e09261658afb3&query=${search}&page=1`)
        .then((result) => {
            dispatch(searchMovies(result.data.results))
        })
}

export const movieDetails = (movieDetails: any) => ({ type: GET_MOVIE_DETAILS, movieDetails: movieDetails })


export const getmovieDetails = (id: string) => (dispatch: any) => {
    axios(`https://api.themoviedb.org/3/movie/${id}?api_key=138a341d99d20356c33e09261658afb3&language=pt-BR&append_to_response=videos`)
        .then((result) => {
            dispatch(movieDetails(result.data))
        })
}