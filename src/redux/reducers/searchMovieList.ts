import { GET_SEARCH_MOVIES } from '../actions/type'

const defaultState = { searchMovies: [] }

const searchMovies = (state = defaultState, actions: any) => {
    switch (actions.type) {
        case GET_SEARCH_MOVIES:
            return {
                ...state,
                searchMovies: actions.searchMovieList
            };

        default:
            return state
    }
}

export default searchMovies;