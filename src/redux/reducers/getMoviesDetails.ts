import { GET_MOVIE_DETAILS } from '../actions/type'

const defaultState = { movieDetails: {} }

const movieDetails = (state = defaultState, actions: any) => {
    switch (actions.type) {
        case GET_MOVIE_DETAILS:
            return {
                ...state,
                movieDetails: actions.movieDetails
            };

        default:
            return state
    }
}

export default movieDetails;