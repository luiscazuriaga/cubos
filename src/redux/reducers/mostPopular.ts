import { GET_MOST_POPULAR_MOVIES } from '../actions/type'

const defaultState = { mostPopularMovies: [] }

const mostPopularMovies = (state = defaultState, actions: any) => {
    switch (actions.type) {
        case GET_MOST_POPULAR_MOVIES:
            return {
                ...state,
                mostPopularMovies: actions.popularMovieList
            };

        default:
            return state
    }
}

export default mostPopularMovies;
