import { combineReducers } from 'redux'
import mostPopularMovies from './mostPopular'
import searchMovies from './searchMovieList'
import movieDetails from './getMoviesDetails'

export default combineReducers({ mostPopularMovies, searchMovies, movieDetails });