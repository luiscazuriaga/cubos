import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import HomeCard from '../../components/HomeCard/HomeCard';
import Pagination from '../../components/Pagination/Pagination';
import { getMostPopularMovies, getSearchMovies } from '../../redux/actions/actions';
import {
    Container,
    BoxInput,
    BoxFilms
} from './style'


const Home = () => {
    const mostPopularMovies = useSelector((state: any) => state.mostPopularMovies.mostPopularMovies)
    const searchMovies = useSelector((state: any) => state.searchMovies.searchMovies)
    const dispatch = useDispatch()
    const [currentMovies, setCurrentMovies] = useState([])
    const [currentPage, setCurrePage] = useState(1)
    const [input, setInput] = useState("")

    const serchMovie = (target: any) => {
        setCurrentMovies([])
        setInput(target.value)
    }


    useEffect(() => {
        mostPopularMovies.length === 0 &&
            dispatch(getMostPopularMovies())
        setCurrentMovies(mostPopularMovies)
    }, [dispatch, mostPopularMovies])

    useEffect(() => {
        if (currentMovies.length === 0 && input !== "") {
            dispatch(getSearchMovies(input))
            setCurrentMovies(searchMovies)
        }
    }, [dispatch, input, currentMovies, searchMovies])

    const setPage = (page: number) => setCurrePage(page)

    const lastMovie = currentPage * 5
    const currentMovieList = currentMovies.slice(lastMovie - 5, lastMovie);
    return (
        <Container>
            <BoxInput>
                <input type="text" placeholder="Busque um filme por nome, ano ou gênero..." onChange={({ target }) => serchMovie(target)} />
            </BoxInput>
            <BoxFilms>
                {currentMovieList.map((movie: any, key: any) => (<HomeCard movie={movie} key={key} />))}
                <Pagination currentMovies={currentMovies.length} setPage={setPage} currentPage={currentPage} />
            </BoxFilms>
        </Container>
    )
}

export default Home

