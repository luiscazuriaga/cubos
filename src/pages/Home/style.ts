import styled from 'styled-components'

export const Container = styled.div`
width: 100%;
display:flex;
justify-content:center;
align-items:center;
flex-flow: column;
`

export const BoxInput = styled.div`
width: 90%;
height: 50px;
display:flex;
justify-content:center;
align-items:center;
background:blue;
border-radius: 24px;
background: var(--old-white);
margin: 50px 0 50px 0;

input{
background:none;
border:none;
outline:none;
width: 95%;
height: 100%;
font-family: "Abel",sans-serif;
font-size: 20px;
}
input::placeholder{
color: var(--placeholder);
}
`

export const BoxFilms = styled.div`
width: 90%;
height: 100vh;
`