import React from 'react';
import styled from 'styled-components'
import Routes from './routes';

function App() {


  return (
    <Container>
      <Header>Movies</Header>
      <Routes />
    </Container>
  );
}

export default App;

const Container = styled.div`
display:flex;
justify-content:center;
align-items:center;
flex-flow: column;
`

const Header = styled.div`
display:flex;
justify-content:center;
align-items:center;
width: 100%;
height: 100px;
background:var(--main-blue);
font-family: "Abel", sans-serif;
font-size: 3rem;
color:var(--main-cyan);
`