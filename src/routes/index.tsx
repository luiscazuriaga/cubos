import React from "react"
import { Switch, Route } from "react-router-dom"
import Home from "../pages/Home/Home";
import DetailsCard from '../components/DetailsCard/DetailsCard'
const Routes = () => {
    return (
        <Switch>
            <Route exact path="/">
                <Home />
            </Route>
            <Route exact path="/detalhes/:id">
                <DetailsCard />
            </Route>
        </Switch>
    )
}

export default Routes;