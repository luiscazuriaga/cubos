import styled from 'styled-components'

export const Container = styled.div`
width: 90%;
height: 500px;
margin-top:100px;
display:flex;
justify-content:center;
align-items:center;
flex-flow:column;
background: var(--old-white);

@media(min-width:768px){
height: 350px;
}
@media(min-width:1024px){
height: 500px;
}
`

export const Infos = styled.div`
    flex: 5;
    height: 100%;
    display:flex;
    justify-content:center;
    align-items:center;
    flex-flow: column;
    max-width:150px;
    box-sizing: border-box;
    background-color: var(--old-white-ligth);
section{
    width:90%;
    height:25%;
    display:flex;
    justify-content:start;
    align-items:center;
    flex-flow: column;
    box-sizing: border-box;

}
section:first-child{
    height:50%
}

section:last-child{
    flex-flow: row;
}

h1{
    font-family: "Abel", sans-serif;
    font-weight: lighter;
    width:100%;
    border-bottom: 2px solid var(--main-cyan); 
    margin-top:20px;
    color: var(--main-blue);
}    

h2{
    font-family: "Abel", sans-serif;
    font-weight: lighter;
    width:100%;
    color: var(--main-blue);
}
p{
    margin-top:10px;
    width:100%;
}
@media(min-width:408px){
    max-width:none;
}
`


export const Banner = styled.div`
    flex: 1 1 0px;
    height: 100%;
    display:flex;
    justify-content:center;
    align-items:center;
    flex-flow:column;
    align-self: flex-end;
img{
    width: 100%;    
    height: 100%;   
}
@media(min-width:768px){
    flex: 1 1 200px;    
}
@media(min-width:1024px){
    flex: 1 1 250px;    
}
`
export const BoxCircle = styled.span`
    flex:1;
    display:flex;
    justify-content:center;
    align-items:center;
    flex-flow: column;
    color: var(--main-cyan);
    
    span{
        width:25px;
        height:25px;
    }
@media(min-width: 420px){
    span{
        width:29px;
        height:29px;
    }
}    
@media(min-width:768px){
span{
   transform: scale(2);
}
}
@media(min-width: 1220px){
    span{
   transform: scale(3);
    }
}
`
export const Title = styled.span`
    width: 93%;
    height:15%;
    display:flex;
    justify-content: space-between;
    align-items: center;
    color: var(--main-blue);
    background-color: var(--old-white);

h1, h2{
    font-size:1.1rem;
    font-family: "Abel", sans-serif;
    font-weight: lighter;
}
h2{
    font-size:0.5rem;
    color: gray;
    font-family: "Lato", sans-serif;
}

@media(min-width:768px){
span{
   transform: scale(2);

}
h1{
    font-size:2.4rem;
}
h2{
    font-size:1.2rem;
}
}
@media(min-width: 1220px){
    span{
   transform: scale(3);
    }
h1{
    font-size:3rem;
}
h2{
    font-size:1.5rem;
}
}
`

export const BoxGenre = styled.div`
flex:4;
height:100%;
display:flex;
justify-content:start;
align-items:start;

div{
    margin-left:10px;
}
`


export const BoxContent = styled.div`
display:flex;
justify-content:center;
align-items:center;
width:100%;
height:85%
`
export const BoxTitles = styled.div`
display:flex;
justify-content:space-between;
align-items:center;
width:100%
`