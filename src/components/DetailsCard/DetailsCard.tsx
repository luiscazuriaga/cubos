import React, { useEffect, useState } from 'react';
import GenreTag from '../GenreTag/GenreTag';
import PercentageCircle from '../Circle/Circle';

import {
    Container,
    Banner,
    Infos,
    BoxCircle,
    Title,
    BoxGenre,
    BoxTitles,
    BoxContent
} from './style'
import { Link, useParams } from 'react-router-dom';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { getmovieDetails } from '../../redux/actions/actions';

interface Props {
    movie: any
}

// let a = {
//     adult: false,
//     backdrop_path: "/8rIoyM6zYXJNjzGseT3MRusMPWl.jpg",
//     genre_ids: [14, 10751, 12, 35, 27],
//     id: 531219,
//     original_language: "en",
//     original_title: "Roald Dahl's The Witches",
//     overview: "Um menino acaba descobrindo uma conferência de bruxas enquanto fica com a avó em um hotel e é transformado em rato pela Grande Bruxa.",
//     popularity: 2403.297,
//     poster_path: "/kQDfB6dPYoiuUtbSYusNopSXyVp.jpg",
//     release_date: "2020-10-22",
//     title: "Convenção das Bruxas",
//     video: false,
//     vote_average: 7.1,
//     vote_count: 569,
// }

const DetailsCard = () => {
    const movieDetails = useSelector((state: any) => state.movieDetails.movieDetails)
    const dispatch = useDispatch()
    const { id }: { id: string } = useParams()
    const { title, overview, genres, release_date, poster_path, vote_average } = movieDetails
    const ImageURL = `https://image.tmdb.org/t/p/w600_and_h900_bestv2${poster_path}`

    useEffect(() => {
        !movieDetails.hasOwnProperty("id") &&
            dispatch(getmovieDetails(id))
    }, [dispatch, id, movieDetails])

    return (
        <Container>
            <Title>
                <h1>{title}</h1>
                <h2>{release_date && release_date.replace(/-/g, "/")}</h2>
            </Title>
            <BoxContent>
                <Infos>
                    <section>
                        <h1>Sinopse</h1>
                        <p>{overview}</p>
                    </section>
                    <section>
                        <h1>Informações</h1>
                        <BoxTitles>
                            <h2>Situação</h2>
                            <h2>Idioma</h2>
                            <h2>Duração</h2>
                            <h2>Orçamento</h2>
                            <h2>Receita</h2>
                            <h2>Lucro</h2>
                        </BoxTitles>
                    </section>
                    <section>
                        <BoxGenre>
                            {genres && genres.map((elem: any, key: number) =>
                                <GenreTag genre={elem.name} key={key} />
                            )}
                        </BoxGenre>
                        <BoxCircle>
                            <span>
                                <PercentageCircle text={`${vote_average * 10}%`} />
                            </span>
                        </BoxCircle>
                    </section>
                </Infos>
                <Banner>
                    <img src={poster_path && ImageURL} alt={"title"} />
                </Banner>
            </BoxContent>
        </Container>
    )
}

export default DetailsCard
