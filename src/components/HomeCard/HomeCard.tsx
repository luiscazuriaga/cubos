import React from 'react';
import GenreTag from '../GenreTag/GenreTag';
import PercentageCircle from '../Circle/Circle';

import {
    Container,
    Banner,
    Infos,
    InfoHeader,
    BoxCircle,
    BoxTitle,
    InfoMain,
    BoxGenre,
    StyledLink
} from './style'
import { Link } from 'react-router-dom';

interface Props {
    movie: any
}

// let a = {
//     adult: false,
//     backdrop_path: "/8rIoyM6zYXJNjzGseT3MRusMPWl.jpg",
//     genre_ids: [14, 10751, 12, 35, 27],
//     id: 531219,
//     original_language: "en",
//     original_title: "Roald Dahl's The Witches",
//     overview: "Um menino acaba descobrindo uma conferência de bruxas enquanto fica com a avó em um hotel e é transformado em rato pela Grande Bruxa.",
//     popularity: 2403.297,
//     poster_path: "/kQDfB6dPYoiuUtbSYusNopSXyVp.jpg",
//     release_date: "2020-10-22",
//     title: "Convenção das Bruxas",
//     video: false,
//     vote_average: 7.1,
//     vote_count: 569,
// }

const HomeCard = ({ movie }: Props) => {
    const { id, title, overview, genre_ids, release_date, poster_path, vote_average } = movie
    const ImageURL = `https://image.tmdb.org/t/p/w600_and_h900_bestv2${poster_path}`
    return (
        <StyledLink to={`/detalhes/${id}`}>
            <Container>
                <Banner>
                    <img src={ImageURL} alt={title} />
                </Banner>
                <Infos>
                    <InfoHeader>
                        <BoxCircle>
                            <span>
                                <PercentageCircle text={`${vote_average * 10}%`} />
                            </span>
                        </BoxCircle>
                        <BoxTitle>
                            <h1>{title}</h1>
                            <h2>{release_date && release_date.replace(/-/g, "/")}</h2>
                        </BoxTitle>
                    </InfoHeader>
                    <InfoMain>
                        <p>{overview}</p>
                        <BoxGenre>
                            {genre_ids && genre_ids.map((elem: number, key: number) =>
                                <GenreTag genre={elem} key={key} />
                            )}
                        </BoxGenre>


                    </InfoMain>
                </Infos>
            </Container>
        </StyledLink>
    )
}

export default HomeCard

