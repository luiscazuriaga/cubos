

import { Link } from 'react-router-dom'
import styled from 'styled-components'

export const Container = styled.div`
width: 100%;
height: 300px;
margin-bottom:50px;
display:flex;
justify-content:center;
align-items:center;
background: green;

@media(min-width:768px){
height: 350px;
}
@media(min-width:1024px){
height: 500px;
}
`

export const Banner = styled.div`
    flex: 1 1 0px;
    height: 100%;
    display:flex;
    justify-content:center;
    align-items:center;
    flex-flow:column;
    align-self: flex-end;
img{
    width: 100%;    
    height: 100%;   
}
@media(min-width:768px){
    flex: 1 1 200px;    
}
@media(min-width:1024px){
    flex: 1 1 250px;    
}
`

export const Infos = styled.div`
    flex: 5;
    height: 100%;
    display:flex;
    justify-content:center;
    align-items:center;
    flex-flow: column;
    background: lawngreen;
    max-width:150px;
    
@media(min-width:408px){
    max-width:none;

}

`
export const InfoHeader = styled.div`
    box-sizing: border-box;
    display:flex;
    width: 100%;
    height: 40%;
    background-image: linear-gradient(var(--main-blue)50%,var(--old-white)50%);

    span{
        height: 100%;
    }
`
export const BoxCircle = styled.span`
    width: 22%;
    display:flex;
    justify-content:center;
    align-items:center;
    flex-flow: column;
    color: var(--main-cyan);
    
    span{
        width:25px;
        height:25px;
    }
@media(min-width: 420px){
    span{
        width:40px;
        height:40px;
    }
}    
@media(min-width:768px){
span{
   transform: scale(2);
}
}
@media(min-width: 1220px){
    span{
   transform: scale(3);
    }
}
`
export const BoxTitle = styled.span`
    width: 79%;
    display:flex;
    justify-content:center;
    flex-flow: column;
    color: var(--main-cyan);
    padding-left:5px;
    text-overflow: ellipsis;
    overflow: hidden;

h1, h2{
    font-size:1.1rem;
    font-family: "Abel", sans-serif;
    font-weight: lighter;
    
}
h1{
    text-overflow: ellipsis;
    overflow: hidden;  
    white-space: nowrap;
    width: 100%;
}

h2{
    margin-bottom:0.9rem;
    font-size:0.5rem;
    color: gray;
    font-family: "Lato", sans-serif;
}

@media(min-width:768px){
span{
   transform: scale(2);

}
h1{
    font-size:2.4rem;
}
h2{
    font-size:1.2rem;
    margin-bottom:1.5rem;

}
}
@media(min-width: 1220px){
    span{
   transform: scale(3);

    }
h1{
    font-size:3rem;
}
h2{
    font-size:1.5rem;
    margin-bottom:2rem;
}
}
`


export const InfoMain = styled.div`
width: 100%;
height:60%;
background:var(--old-white);
display:flex;
align-items:center;
flex-flow: column;

p{
    width:80%;
    font-size:0.5rem;
    color:black;
}

@media(min-width:768px){
    p{
    font-size:1.2rem;
}
}
@media(min-width: 1220px){
    p{
    font-size:1.2rem;
}
}
`

export const BoxGenre = styled.div`
width:80%;
height: 60px;
display:flex;
justify-content:end;
align-items:center;
flex-flow: wrap;
`

export const StyledLink = styled(Link)`
text-decoration:none;
color:none;
`