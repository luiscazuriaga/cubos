import React from 'react'
import styled from 'styled-components'

interface Props {
    text: string;
    active?: boolean;
}

interface StyledProps {
    active?: boolean;
}

const Circle = ({ text, active }: Props) => {

    return (
        <Box active={active === undefined ? true : active}>
            <div>
                <p>{text}</p>
            </div>
        </Box>
    )
}

export default Circle


const Box = styled.div<StyledProps>`
    width: 100%;
    height: 100%;
    border-radius:50%;
    background: var(--main-blue);
    display:flex;
    justify-content:center;
    align-items:center;
    transition: 0.2s;
    transform: scale(1.2);

div{
    width: 80%;
    height: 80%;
    display:flex;
    justify-content:center;
    align-items:center;
    border-radius:50%;
    border: 1px var(--main-cyan) solid;
}

p{
    display:flex;
    justify-content:center;
    align-items:center;
    margin:0 !important;
    padding: 0 !important;
    font-size:0.6rem;
    font-family: "Abel", sans-serif;
    color: var(--main-cyan);

}

@media(min-width:400px){
    div{
    border: 1px var(--main-cyan) solid;
}
    p{
    font-size:0.7rem;
}
}

${({ active }) => !active && `
    background: none;
    transform: scale(1);

div{
    border: none;
}
p{
    color:black;

}
`}

`