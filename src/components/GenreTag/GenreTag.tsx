import React from 'react'
import styled from 'styled-components'
import genres from './helper'

interface Props {
    genre: any;
}

const GenreTag = ({ genre }: Props) => {

    const genreFind = genres.find((item) => item.id === genre)
    return (
        <Tag>
            <p>{genreFind !== undefined ? genreFind!.name : genre}</p>
        </Tag>
    )
}

export default GenreTag

const Tag = styled.div`
box-sizing: border-box;
margin-right:2px;
border-radius:24px;
border: 1px solid var(--main-blue);
color: var(--main-blue) !important;
font-family: "Abel", sans-serif;
padding:3px;

p{
    margin:0 !important;
    color: var(--main-blue) !important;
    white-space: nowrap;
}
`