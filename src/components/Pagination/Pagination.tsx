import { stat } from 'fs'
import React, { useState } from 'react'
import styled from 'styled-components'
import Circle from '../Circle/Circle'

interface Props {
    currentMovies: number
    setPage: (page: number) => void
    currentPage: number
}
const Pagination = ({ currentMovies, setPage, currentPage }: Props) => {
    const pageIndex = Array(Math.ceil(currentMovies / 5)).fill(1).map((elem, index) => elem + index)

    return (
        <Container>
            {pageIndex.map((number, key) => (
                <span onClick={() => setPage(number)} key={key}>
                    <Circle text={number} active={number === currentPage} />
                </span>
            ))}
        </Container>
    )

}

export default Pagination

const Container = styled.div`
width:40%;
display:flex;
justify-content: space-between;
align-items:center;
margin: 0 auto;

span{
    width:28px;
    height:28px;
}

@media(min-width:768px){
span{
   transform: scale(2);
}
}

@media(min-width: 1220px){
span{
   transform: scale(3);
}
}
`